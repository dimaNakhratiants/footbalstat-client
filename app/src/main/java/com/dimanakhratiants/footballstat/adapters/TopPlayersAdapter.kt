package com.dimanakhratiants.footballstat.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.entity.Player

/**
 * Created by dima on 29.11.2017.
 */

class TopPlayerHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    fun setPlayer(player: Player){
        itemView.findViewById<TextView>(R.id.playerNameTextView).setText(player.name)
        itemView.findViewById<TextView>(R.id.teamTitleTextView).setText(player.teamTitle)
        itemView.findViewById<TextView>(R.id.goalsTextView).setText(player.goals.count().toString())
    }
}


class TopPlayersAdapter(var players: List<Player> = ArrayList()): RecyclerView.Adapter<TopPlayerHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TopPlayerHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_scorer, parent, false)
        val holder = TopPlayerHolder(view)
        return holder
    }

    override fun onBindViewHolder(holder: TopPlayerHolder?, position: Int) {
        holder?.setPlayer(players[position])
    }

    override fun getItemCount(): Int {
        return players.count()
    }
}