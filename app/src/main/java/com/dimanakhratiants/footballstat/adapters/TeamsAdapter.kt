package com.dimanakhratiants.footballstat.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.entity.Team


/**
 * Created by dima on 29.11.2017.
 */

class TeamHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    fun setTeam(team: Team){
        itemView.findViewById<TextView>(R.id.titleTextView).setText(team.title)
    }
}


class TeamsAdapter(var teams: ArrayList<Team> = ArrayList(),
                   val listener: TeamClickListener,
                   val deleteListener: TeamDeleteListener): RecyclerView.Adapter<TeamHolder>(){

    interface TeamDeleteListener{
        fun onTeamDelete(team: Team)
    }

    interface TeamClickListener{
        fun onTeamClick(team: Team)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): TeamHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_team, parent, false)
        val holder = TeamHolder(view)
        return holder
    }

    override fun onBindViewHolder(holder: TeamHolder?, position: Int) {
        holder?.setTeam(teams[position])
        holder?.itemView?.setOnClickListener {
            listener.onTeamClick(teams[position])
        }
        holder?.itemView?.findViewById<ImageView>(R.id.deleteView)?.setOnClickListener {
            deleteListener.onTeamDelete(teams[position])
        }
    }

    override fun getItemCount(): Int {
        return teams.count()
    }
}