package com.dimanakhratiants.footballstat.adapters

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.dimanakhratiants.footballstat.fragments.ListFragment
import com.dimanakhratiants.footballstat.fragments.MainFragment

/**
 * Created by dima on 29.11.2017.
 */

class ViewPagerAdapter(fm: FragmentManager): FragmentStatePagerAdapter(fm){
    override fun getItem(position: Int): Fragment {
        return when (position)  {
            0 -> MainFragment.newInstance()
            1 -> ListFragment.newInstance(ListFragment.Type.TEAM)
            else -> ListFragment.newInstance(ListFragment.Type.FIXTURE)
        }
    }

    override fun getCount(): Int {
        return 3
    }
}