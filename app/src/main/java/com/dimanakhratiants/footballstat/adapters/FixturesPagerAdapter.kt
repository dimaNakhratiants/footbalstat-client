package com.dimanakhratiants.footballstat.adapters

import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.entity.Fixture
import com.dimanakhratiants.footballstat.model.Model
import kotlinx.android.synthetic.main.activity_fixture.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by dima on 29.11.2017.
 */
class FixturesPagerAdapter(val fixtures: List<Fixture>, val listener: OnFixtureClickListener): PagerAdapter(){


    var views: ArrayList<View> = ArrayList()

    interface OnFixtureClickListener{
        fun onFixtureClick(fixture: Fixture)
    }

    override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup?, position: Int): Any {
        val view = LayoutInflater.from(container?.context).inflate(R.layout.item_fixture, container, false)
        view.findViewById<TextView>(R.id.homeTeamTextView).setText(fixtures[position].homeTeam?.title)
        view.findViewById<TextView>(R.id.awayTeamTextView).setText(fixtures[position].awayTeam?.title)
        val date = Date(fixtures[position].date)
        view.findViewById<TextView>(R.id.dateTextView).setText(Model.format.format(date))

        view.findViewById<ImageView>(R.id.deleteView).visibility = View.GONE
        setUpScore(fixtures[position],
                view.findViewById<TextView>(R.id.homeGoalsTextView),
                view.findViewById<TextView>(R.id.awayGoalsTextView))

        view.setOnClickListener {
            listener.onFixtureClick(fixtures[position])
        }

        container?.addView(view)
        views.add(view)
        return view
    }

    private fun setUpScore(fixture: Fixture, homeGoalsTextView: TextView, awayGoalsTextView: TextView  ){
        val homeGoals = fixture.goals.filter { fixture.homeTeam?.players?.map { it.name }!!.contains(it.playerName) }
        val awayGoals = fixture.goals.filter { fixture.awayTeam?.players?.map { it.name }!!.contains(it.playerName) }
        homeGoalsTextView.setText(homeGoals.count().toString())
        awayGoalsTextView.setText(awayGoals.count().toString())
    }

    override fun destroyItem(container: ViewGroup?, position: Int, `object`: Any?) {
        container?.removeView(views[position])
    }
    override fun getCount(): Int {
        return fixtures.count()
    }
}