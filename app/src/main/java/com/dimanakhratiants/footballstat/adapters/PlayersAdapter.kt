package com.dimanakhratiants.footballstat.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.entity.Player
import com.dimanakhratiants.footballstat.entity.Position
import com.dimanakhratiants.footballstat.model.Model
import java.util.*

/**
 * Created by dima on 30.11.2017.
 */
class PlayerHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    fun setPlayer(player: Player){
        itemView.findViewById<TextView>(R.id.nameTextView).setText(player.name)
        itemView.findViewById<TextView>(R.id.dateTextView).setText(Model.format.format(Date(player.dateOfBirth)))
        itemView.findViewById<TextView>(R.id.nationTextView).setText(player.nationality)
        itemView.findViewById<TextView>(R.id.positionTextView).setText(Position.valueOf(player.position).abbr())
    }
}


class PlayersAdapter(var players: ArrayList<Player> = ArrayList(),
                     var deleteListener: PlayerDeleteListener): RecyclerView.Adapter<PlayerHolder>(){

    interface PlayerDeleteListener{
        fun onPlayerDelete(player: Player)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): PlayerHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_player, parent, false)
        val holder = PlayerHolder(view)
        return holder
    }

    override fun onBindViewHolder(holder: PlayerHolder?, position: Int) {
        holder?.setPlayer(players[position])
        holder?.itemView?.findViewById<ImageView>(R.id.deleteView)?.setOnClickListener {
            deleteListener.onPlayerDelete(players[position])
        }
    }

    override fun getItemCount(): Int {
        return players.count()
    }
}