package com.dimanakhratiants.footballstat.adapters

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.entity.Fixture
import com.dimanakhratiants.footballstat.entity.Goal
import com.dimanakhratiants.footballstat.entity.Team
import java.util.*
import kotlin.Comparator

/**
 * Created by dima on 30.11.2017.
 */

class GoalHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun setTeam(goal: Goal, fixture: Fixture) {
        itemView.findViewById<TextView>(R.id.minuteTextView).setText("${goal.minute}'")

        val homeGoalPlayer = itemView.findViewById<TextView>(R.id.homeGoalPlayer)
        val awayGoalPlayer = itemView.findViewById<TextView>(R.id.awayGoalPlayer)

        Log.d("GOAL", goal.toString())
        homeGoalPlayer.setText(" ")
        awayGoalPlayer.setText(" ")

        fixture.homeTeam?.let {
            if (it.players?.map { it.name }?.contains(goal.playerName)) {
                homeGoalPlayer.setText(goal.playerName)
            } else {
                homeGoalPlayer.setText(" ")
            }
        }
        fixture?.awayTeam?.let {
            if (it.players?.map { it.name }?.contains(goal.playerName)) {
                awayGoalPlayer.setText(goal.playerName)
            } else {
                awayGoalPlayer.setText(" ")
            }
        }


    }
}


class GoalsAdapter(val fixture: Fixture,
                   val deleteListener: GoalDeleteListener) : RecyclerView.Adapter<GoalHolder>() {

    var goals: ArrayList<Goal> = ArrayList()

    interface GoalDeleteListener{
        fun onGoalDelete(goal: Goal)
    }
    init {
        goals.addAll(fixture.goals)
        goals.sortBy { it.minute }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): GoalHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_goal, parent, false)
        view.findViewById<TextView>(R.id.homeGoalPlayer).text = ""
        view.findViewById<TextView>(R.id.awayGoalPlayer).text = ""
        val holder = GoalHolder(view)
        return holder
    }

    override fun onBindViewHolder(holder: GoalHolder?, position: Int) {
        holder?.setTeam(goals[position], fixture)
        holder?.itemView?.findViewById<ImageView>(R.id.deleteView)?.setOnClickListener {
            deleteListener.onGoalDelete(goals[position])
        }
    }

    override fun getItemCount(): Int {
        return goals.count()
    }
}