package com.dimanakhratiants.footballstat.adapters

import android.media.Image
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.entity.Fixture
import com.dimanakhratiants.footballstat.entity.Team
import com.dimanakhratiants.footballstat.model.Model
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by dima on 29.11.2017.
 */
class FixturesHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    fun setFixture(fixture: Fixture){
        itemView.findViewById<TextView>(R.id.homeTeamTextView).setText(fixture.homeTeam?.title)
        itemView.findViewById<TextView>(R.id.awayTeamTextView).setText(fixture.awayTeam?.title)
        setUpScore(fixture,
                itemView.findViewById(R.id.homeGoalsTextView),
                itemView.findViewById(R.id.awayGoalsTextView))

        val date = Date(fixture.date)
        itemView.findViewById<TextView>(R.id.dateTextView).setText(Model.format.format(date))
    }

    private fun setUpScore(fixture: Fixture, homeGoalsTextView: TextView, awayGoalsTextView: TextView  ){
        val homeGoals = fixture.goals.filter { fixture.homeTeam?.players?.map { it.name }!!.contains(it.playerName) }
        val awayGoals = fixture.goals.filter { fixture.awayTeam?.players?.map { it.name }!!.contains(it.playerName) }
        homeGoalsTextView.setText(homeGoals.count().toString())
        awayGoalsTextView.setText(awayGoals.count().toString())
    }
}


class FixturesAdapter(var fixtures: ArrayList<Fixture> = ArrayList(),
                      var listener: FixtureClickListener,
                      var deleteListener: FixtureDeleteListener): RecyclerView.Adapter<FixturesHolder>(){

    interface FixtureDeleteListener{
        fun onFixtureDelete(fixture: Fixture)
    }

    interface FixtureClickListener{
        fun onFixtureClick(fixture: Fixture)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): FixturesHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_fixture, parent, false)
        val holder = FixturesHolder(view)
        return holder
    }

    override fun onBindViewHolder(holder: FixturesHolder?, position: Int) {
        val fixture = fixtures[position]
        holder?.setFixture(fixture)
        holder?.itemView?.setOnClickListener {
            listener.onFixtureClick(fixture)
        }

        holder?.itemView?.findViewById<ImageView>(R.id.deleteView)?.setOnClickListener {
            deleteListener.onFixtureDelete(fixture)
        }

    }

    override fun getItemCount(): Int {
        return fixtures.count()
    }
}