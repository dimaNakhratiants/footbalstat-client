package com.dimanakhratiants.footballstat.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.entity.Team
import com.dimanakhratiants.footballstat.model.Model
import com.google.gson.Gson
import com.google.gson.internal.bind.ArrayTypeAdapter
import kotlinx.android.synthetic.main.dialog_add_fixture.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async

/**
 * Created by dima on 01.12.2017.
 */
class AddTeamDialog: DialogFragment() {

    var listener: AddTeamListener? = null

    interface AddTeamListener{
        fun addTeam(team: Team)
    }

    companion object {
        fun newInstance(listener: AddTeamListener): AddTeamDialog{
            val dialog = AddTeamDialog()
            dialog.listener = listener
            return dialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.dialog_add_team, container, false)
        val titleEditText = view?.findViewById<EditText>(R.id.titleEditText)

        view?.findViewById<Button>(R.id.okBtn)?.setOnClickListener {
            val team = Team(title = titleEditText?.text.toString())
            listener?.addTeam(team)
            this.dismiss()
        }
        view?.findViewById<Button>(R.id.cancelButton)?.setOnClickListener { this.dismiss() }
        return view
    }


}