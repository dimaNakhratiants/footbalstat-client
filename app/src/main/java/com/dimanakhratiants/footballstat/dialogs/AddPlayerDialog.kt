package com.dimanakhratiants.footballstat.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.entity.Fixture
import com.dimanakhratiants.footballstat.entity.Player
import com.dimanakhratiants.footballstat.entity.Position
import com.dimanakhratiants.footballstat.entity.Team
import com.dimanakhratiants.footballstat.model.Model
import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by dima on 01.12.2017.
 */
class AddPlayerDialog: DialogFragment() {

    var listener: AddPlayerListener? = null
    var team: Team? = null

    interface AddPlayerListener{
        fun addPlayer(player: Player)
    }

    companion object {
        fun newInstance(listener: AddPlayerListener, team: Team): AddPlayerDialog{
            val dialog = AddPlayerDialog()
            dialog.listener = listener
            dialog.team = team
            return dialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.dialog_add_player, container, false)
        val dateText = view?.findViewById<EditText>(R.id.dateTextView)
        val nationText = view?.findViewById<EditText>(R.id.nationText)
        val positionSpinner = view?.findViewById<Spinner>(R.id.positionSpinner)
        val nameText = view?.findViewById<EditText>(R.id.nameText)

        val positions = Position.values().map { it.name }

        positionSpinner?.adapter = ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, positions)

        view?.findViewById<Button>(R.id.okBtn)?.setOnClickListener {
            val player = Player(
                    name = nameText?.text.toString(),
                    nationality = nationText?.text.toString(),
                    position = positionSpinner?.selectedItem.toString(),
                    dateOfBirth = Model.format.parse(dateText?.text.toString()).time,
                    team = this.team
            )
            listener?.addPlayer(player)
            this.dismiss()
        }
        view?.findViewById<Button>(R.id.cancelButton)?.setOnClickListener { this.dismiss() }
        return view
    }


}