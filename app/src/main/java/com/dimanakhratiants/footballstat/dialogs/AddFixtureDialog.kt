package com.dimanakhratiants.footballstat.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.entity.Fixture
import com.dimanakhratiants.footballstat.entity.Team
import com.dimanakhratiants.footballstat.model.Model
import com.google.gson.Gson
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by dima on 01.12.2017.
 */
class AddFixtureDialog: DialogFragment() {

    var listener: AddFixtureListener? = null

    interface AddFixtureListener{
        fun addFixture(fixture: Fixture)
    }

    companion object {
        fun newInstance(listener: AddFixtureListener): AddFixtureDialog{
            val dialog = AddFixtureDialog()
            dialog.listener = listener
            return dialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.dialog_add_fixture, container, false)
        val homeTeamSpinner = view?.findViewById<Spinner>(R.id.spinner)
        val awayTeamSpinner = view?.findViewById<Spinner>(R.id.spinner3)
        val dateTextView = view?.findViewById<TextView>(R.id.dateTextView)
        var teams = ArrayList<Team>()
        async(UI) {
            teams = async { Model.getTeams() }.await() as ArrayList
            val adapter = ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, teams.map { it.title })
            homeTeamSpinner?.adapter = adapter
            awayTeamSpinner?.adapter = adapter
        }
        view?.findViewById<Button>(R.id.okBtn)?.setOnClickListener {
            val fixture = Fixture(date = Model.format.parse(dateTextView?.text.toString()).time,
                    homeTeam = teams.find { it.title == homeTeamSpinner?.selectedItem.toString()},
                    awayTeam = teams.find { it.title == awayTeamSpinner?.selectedItem.toString()}
            )
            listener?.addFixture(fixture)
            this.dismiss()
        }
        view?.findViewById<Button>(R.id.cancelButton)?.setOnClickListener { this.dismiss() }
        return view
    }


}