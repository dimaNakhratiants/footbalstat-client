package com.dimanakhratiants.footballstat.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.entity.*
import kotlin.collections.ArrayList

/**
 * Created by dima on 01.12.2017.
 */
class AddGoalDialog: DialogFragment() {

    var listener: AddGoalListener? = null
    var players = ArrayList<Player>()
    var fixture: Fixture? = null

    interface AddGoalListener{
        fun addGoal(goal: Goal)
    }

    companion object {
        fun newInstance(listener: AddGoalListener,  fixture: Fixture): AddGoalDialog{
            val dialog = AddGoalDialog()
            dialog.listener = listener
            dialog.fixture = fixture

            val players = ArrayList<Player>()
            fixture.awayTeam?.players?.let { players.addAll(it) }
            fixture.homeTeam?.players?.let { players.addAll(it) }
            dialog.players = players

            return dialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.dialog_add_goal, container, false)
        val minuteText = view?.findViewById<EditText>(R.id.minuteTextView)
        val playerSpinner = view?.findViewById<Spinner>(R.id.playerSpinnder)

        val positions = Position.values().map { it.name }

        playerSpinner?.adapter = ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, players.map { it.name })

        view?.findViewById<Button>(R.id.okBtn)?.setOnClickListener {
            val goal = Goal(
                    fixture = fixture,
                    minute = minuteText?.text.toString().toInt(),
                    player = players.find { it.name == playerSpinner?.selectedItem.toString()},
                    playerName = playerSpinner?.selectedItem.toString()
            )
            listener?.addGoal(goal)
            this.dismiss()
        }
        view?.findViewById<Button>(R.id.cancelButton)?.setOnClickListener { this.dismiss() }
        return view
    }


}