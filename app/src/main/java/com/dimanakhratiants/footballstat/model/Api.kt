package com.dimanakhratiants.footballstat.model

import com.dimanakhratiants.footballstat.entity.Fixture
import com.dimanakhratiants.footballstat.entity.Goal
import com.dimanakhratiants.footballstat.entity.Player
import com.dimanakhratiants.footballstat.entity.Team
import okhttp3.Response
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by dima on 29.11.2017.
 */

interface Api{

    @GET("players")
    fun getPlayers(): Call<List<Player>>

    @GET("fixtures")
    fun getFixtures(): Call<List<Fixture>>

    @GET("fixtures")
    fun getTeamFixtures(@QueryMap parameters: Map<String, String>): Call<List<Fixture>>

    @GET("teams")
    fun getTeams(): Call<List<Team>>

    @POST("team")
    fun addTeam(@Body team: Team): Call<Team>

    @POST("fixture")
    fun addFixture(@Body fixture: Fixture): Call<Fixture>

    @POST("player")
    fun addPlayer(@Body player: Player): Call<Player>

    @POST("goal")
    fun addGoal(@Body goal: Goal): Call<Goal>

    @POST("team/delete")
    fun deleteTeam(@Body team: Team): Call<Team>

    @POST("fixture/delete")
    fun deleteFixture(@Body fixture: Fixture): Call<Fixture>

    @POST("player/delete")
    fun deletePlayer(@Body player: Player): Call<Player>

    @POST("goal/delete")
    fun deleteGoal(@Body goal: Goal): Call<Goal>

}