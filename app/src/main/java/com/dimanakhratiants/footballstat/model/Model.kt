package com.dimanakhratiants.footballstat.model

import android.util.Log
import com.dimanakhratiants.footballstat.entity.Fixture
import com.dimanakhratiants.footballstat.entity.Goal
import com.dimanakhratiants.footballstat.entity.Player
import com.dimanakhratiants.footballstat.entity.Team
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import kotlin.coroutines.experimental.suspendCoroutine

/**
 * Created by dima on 29.11.2017.
 */

object Model{
    val format = SimpleDateFormat("yyyy-MM-dd")

    val service = Retrofit.Builder()
//            .baseUrl("http://192.168.2.116:8080/")
            .baseUrl("http://10.0.2.2:8080/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(Api::class.java);


    private fun getPlayers(onSuccess: (List<Player>) -> Unit) {
        service.getPlayers().enqueue(object: Callback<List<Player>>{
            override fun onFailure(call: Call<List<Player>>?, t: Throwable?) {
                Log.d("failure", t.toString())

            }
            override fun onResponse(call: Call<List<Player>>?, response: Response<List<Player>>?) {
                response?.body()?.let(onSuccess)
            }
        })
    }

    private fun getTeams(onSuccess: (List<Team>) -> Unit) {
        service.getTeams().enqueue(object: Callback<List<Team>>{
            override fun onFailure(call: Call<List<Team>>?, t: Throwable?) {
                Log.d("failure", t.toString())

            }
            override fun onResponse(call: Call<List<Team>>?, response: Response<List<Team>>?) {
                response?.body()?.let(onSuccess)
            }
        })
    }

    private fun getFixtures(team: String? = null, onSuccess: (List<Fixture>) -> Unit) {
        if (team != null){
            val map = HashMap<String, String>()
            map["team"] = team
            service.getTeamFixtures(map).enqueue(object: Callback<List<Fixture>>{
                override fun onFailure(call: Call<List<Fixture>>?, t: Throwable?) {
                    Log.d("failure", t.toString())
                }
                override fun onResponse(call: Call<List<Fixture>>?, response: Response<List<Fixture>>?) {
                    response?.body()?.let(onSuccess)
                }
            })
        }else{
            service.getFixtures().enqueue(object: Callback<List<Fixture>>{
                override fun onFailure(call: Call<List<Fixture>>?, t: Throwable?) {
                    Log.d("failure", t.toString())

                }
                override fun onResponse(call: Call<List<Fixture>>?, response: Response<List<Fixture>>?) {
                    response?.body()?.let(onSuccess)
                }
            })
        }
    }

    private fun addTeam(team: Team, onSuccess: (Team) -> Unit){
        service.addTeam(team).enqueue(object : Callback<Team> {
            override fun onFailure(call: Call<Team>?, t: Throwable?) {
                Log.d("failure", t.toString())
            }

            override fun onResponse(call: Call<Team>?, response: Response<Team>?) {
                response?.body()?.let(onSuccess)
            }
        })
    }

    private fun addFixture(fixture: Fixture, onSuccess: (Fixture) -> Unit){
        service.addFixture(fixture).enqueue(object : Callback<Fixture> {
            override fun onFailure(call: Call<Fixture>?, t: Throwable?) {
                Log.d("failure", t.toString())
            }

            override fun onResponse(call: Call<Fixture>?, response: Response<Fixture>?) {
                response?.body()?.let(onSuccess)
            }
        })
    }

    private fun addPlayer(player: Player, onSuccess: (Player) -> Unit){
        service.addPlayer(player).enqueue(object : Callback<Player> {
            override fun onFailure(call: Call<Player>?, t: Throwable?) {
                Log.d("failure", t.toString())
            }

            override fun onResponse(call: Call<Player>?, response: Response<Player>?) {
                response?.body()?.let(onSuccess)
            }
        })
    }

    private fun addGoal(goal: Goal, onSuccess: (Goal) -> Unit){
        service.addGoal(goal).enqueue(object : Callback<Goal> {
            override fun onFailure(call: Call<Goal>?, t: Throwable?) {
                Log.d("failure", t.toString())
            }

            override fun onResponse(call: Call<Goal>?, response: Response<Goal>?) {
                response?.body()?.let(onSuccess)
            }
        })
    }

    private fun deleteGoal(goal: Goal, onSuccess: (Goal) -> Unit){
        service.deleteGoal(goal).enqueue(object : Callback<Goal> {
            override fun onFailure(call: Call<Goal>?, t: Throwable?) {
                Log.d("failure", t.toString())
            }

            override fun onResponse(call: Call<Goal>?, response: Response<Goal>?) {
                response?.body()?.let(onSuccess)
            }
        })
    }

    private fun deleteTeam(team: Team, onSuccess: (Team) -> Unit){
        service.deleteTeam(team).enqueue(object : Callback<Team> {
            override fun onFailure(call: Call<Team>?, t: Throwable?) {
                Log.d("failure", t.toString())
            }

            override fun onResponse(call: Call<Team>?, response: Response<Team>?) {
                response?.body()?.let(onSuccess)
            }
        })
    }

    private fun deletePlayer(player: Player, onSuccess: (Player) -> Unit){
        service.deletePlayer(player).enqueue(object : Callback<Player> {
            override fun onFailure(call: Call<Player>?, t: Throwable?) {
                Log.d("failure", t.toString())
            }

            override fun onResponse(call: Call<Player>?, response: Response<Player>?) {
                response?.body()?.let(onSuccess)
            }
        })
    }

    private fun deleteFixture(fixture: Fixture, onSuccess: (Fixture) -> Unit){
        service.deleteFixture(fixture).enqueue(object : Callback<Fixture> {
            override fun onFailure(call: Call<Fixture>?, t: Throwable?) {
                Log.d("failure", t.toString())
            }

            override fun onResponse(call: Call<Fixture>?, response: Response<Fixture>?) {
                Log.d("DELETE", fixture.toString())
                response?.body()?.let(onSuccess)
            }
        })
    }

    suspend fun getPlayers(): List<Player> = suspendCoroutine { continuation ->
        getPlayers{ continuation.resume(it) }
    }

    suspend fun getTeams(): List<Team> = suspendCoroutine { continuation ->
        getTeams(){ continuation.resume(it) }
    }

    suspend fun getFixtures(team: String? = null): List<Fixture> = suspendCoroutine { continuation ->
        if (team != null){
            getFixtures(team){ continuation.resume(it) }
        }else{
            getFixtures{ continuation.resume(it) }
        }
    }

    suspend fun addTeam(team: Team): Team = suspendCoroutine { continuation ->
            addTeam(team){ continuation.resume(it) }
    }

    suspend fun addFixture(fixture: Fixture): Fixture = suspendCoroutine { continuation ->
            addFixture(fixture){ continuation.resume(it) }
    }

    suspend fun addPlayer(player: Player): Player = suspendCoroutine { continuation ->
            addPlayer(player){ continuation.resume(it) }
    }

    suspend fun addGoal(goal: Goal): Goal = suspendCoroutine { continuation ->
            addGoal(goal){ continuation.resume(it) }
    }

    suspend fun deleteTeam(team: Team): Team = suspendCoroutine { continuation ->
            deleteTeam(team){ continuation.resume(it) }
    }

    suspend fun deleteFixture(fixture: Fixture): Fixture = suspendCoroutine { continuation ->
            deleteFixture(fixture){ continuation.resume(it) }
    }

    suspend fun deletePlayer(player: Player): Player = suspendCoroutine { continuation ->
            deletePlayer(player){ continuation.resume(it) }
    }

    suspend fun deleteGoal(goal: Goal): Goal = suspendCoroutine { continuation ->
            deleteGoal(goal){ continuation.resume(it) }
    }


}