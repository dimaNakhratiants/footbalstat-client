package com.dimanakhratiants.footballstat.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.adapters.GoalsAdapter
import com.dimanakhratiants.footballstat.adapters.PlayersAdapter
import com.dimanakhratiants.footballstat.adapters.TeamsAdapter
import com.dimanakhratiants.footballstat.dialogs.AddGoalDialog
import com.dimanakhratiants.footballstat.entity.Fixture
import com.dimanakhratiants.footballstat.entity.Goal
import com.dimanakhratiants.footballstat.model.Model
import kotlinx.android.synthetic.main.activity_fixture.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import java.util.*
import kotlin.collections.ArrayList

class FixtureActivity : AppCompatActivity(), AddGoalDialog.AddGoalListener, GoalsAdapter.GoalDeleteListener {

    lateinit var fixture: Fixture

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fixture)

        fixture = intent.extras["fixture"] as Fixture

        homeTeamTextView.setText(fixture.homeTeam?.title)
        awayTeamTextView.setText(fixture.awayTeam?.title)

        dateTextView.setText(Model.format.format(Date(fixture.date)))

        goalsRecyclerView.layoutManager = LinearLayoutManager(this)

        val adapter = GoalsAdapter(fixture, this)
        goalsRecyclerView.adapter = adapter
        setUpScore()
        floatingActionButton?.setOnClickListener {
            AddGoalDialog.newInstance(this, fixture).show(supportFragmentManager, "ADD GOAL")
        }
    }

    private fun setUpScore(){
        val homeGoals = fixture.goals.filter { fixture.homeTeam?.players?.map { it.name }!!.contains(it.playerName) }
        val awayGoals = fixture.goals.filter { fixture.awayTeam?.players?.map { it.name }!!.contains(it.playerName) }
        homeGoalsTextView.setText(homeGoals.count().toString())
        awayGoalsTextView.setText(awayGoals.count().toString())
    }

    override fun addGoal(goal: Goal) {
        async(UI) {
            val goal = async { Model.addGoal(goal)}.await()
            (fixture.goals as ArrayList).add(goal)
            goalsRecyclerView.adapter = GoalsAdapter(fixture,this@FixtureActivity)
            setUpScore()
        }
    }

    override fun onGoalDelete(goal: Goal) {
        async(UI){
            async { Model.deleteGoal(goal) }.await()
            val adapter = this@FixtureActivity.goalsRecyclerView.adapter as GoalsAdapter
            adapter.goals.remove(goal)
            (this@FixtureActivity.fixture.goals as ArrayList).remove(goal)
            adapter.notifyDataSetChanged()
            setUpScore()
        }
    }
}
