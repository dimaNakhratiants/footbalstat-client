package com.dimanakhratiants.footballstat.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.adapters.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewPager.adapter = ViewPagerAdapter(supportFragmentManager)
        viewPager.offscreenPageLimit = 2
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.getTabAt(0)?.icon = getDrawable(R.drawable.soccer)
        tabLayout.getTabAt(1)?.icon = getDrawable(R.drawable.team)
        tabLayout.getTabAt(2)?.icon = getDrawable(R.drawable.counter)




    }
}
