package com.dimanakhratiants.footballstat.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.adapters.FixturesPagerAdapter
import com.dimanakhratiants.footballstat.adapters.PlayersAdapter
import com.dimanakhratiants.footballstat.adapters.TeamsAdapter
import com.dimanakhratiants.footballstat.dialogs.AddPlayerDialog
import com.dimanakhratiants.footballstat.entity.Fixture
import com.dimanakhratiants.footballstat.entity.Player
import com.dimanakhratiants.footballstat.entity.Team
import com.dimanakhratiants.footballstat.model.Model
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_team.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import java.util.ArrayList

class TeamActivity : AppCompatActivity(), AddPlayerDialog.AddPlayerListener, FixturesPagerAdapter.OnFixtureClickListener,
PlayersAdapter.PlayerDeleteListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_team)

        val team = intent.extras["team"] as Team
        Log.d("team", team.toString())


        playersRecycler.layoutManager = LinearLayoutManager(this)
        val playersAdapter = PlayersAdapter(team.players as ArrayList<Player>, this)
        playersRecycler.adapter = playersAdapter

        async(UI) {
            val fixtures = async { Model.getFixtures(team.title) }.await()
            val adapter = FixturesPagerAdapter(fixtures.sortedBy { it.date }, this@TeamActivity)
            fixturesViewPager.adapter = adapter
            fixturesViewPager.offscreenPageLimit = fixtures.count()
            fixturesViewPager.currentItem = fixtures.indexOf(fixtures.last())
        }

        floatingActionButton.setOnClickListener {
            AddPlayerDialog.newInstance(this, team).show(supportFragmentManager, "ADD PLAYER")
        }
    }

    override fun onFixtureClick(fixture: Fixture) {
        val intent = Intent(this, FixtureActivity::class.java)
        intent.putExtra("fixture", fixture)
        startActivity(intent)
    }

    override fun addPlayer(player: Player) {
        async(UI) {
            val player = async { Model.addPlayer(player)}.await()
            val adapter = playersRecycler.adapter as PlayersAdapter
            adapter.players.add(player)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onPlayerDelete(player: Player) {
        async(UI){
            async { Model.deletePlayer(player) }.await()
            val adapter = this@TeamActivity.playersRecycler.adapter as PlayersAdapter
            adapter.players.remove(player)
            adapter.notifyDataSetChanged()
        }
    }
}
