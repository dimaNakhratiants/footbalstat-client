package com.dimanakhratiants.footballstat.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.activities.FixtureActivity
import com.dimanakhratiants.footballstat.adapters.FixturesPagerAdapter
import com.dimanakhratiants.footballstat.adapters.TopPlayersAdapter
import com.dimanakhratiants.footballstat.entity.Fixture
import com.dimanakhratiants.footballstat.model.Model
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async

/**
 * A simple [Fragment] subclass.
 * Use the [MainFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MainFragment : Fragment(), FixturesPagerAdapter.OnFixtureClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    lateinit var layout: View
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        layout = inflater!!.inflate(R.layout.fragment_main, container, false)


        layout?.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout).setOnRefreshListener {
            updateUI(layout)
            swipeRefreshLayout.isRefreshing = false
        }
        return layout
    }

    override fun onResume() {
        super.onResume()
        updateUI(layout)
    }

    private fun updateUI(view: View) {
        val topScorersRecycler = view.findViewById<RecyclerView>(R.id.playersRecycler)
        topScorersRecycler.layoutManager = LinearLayoutManager(context)
        val topPlayersAdapter = TopPlayersAdapter()

        async(UI) {
            val players = async { Model.getPlayers() }.await()
            topPlayersAdapter.players = players.sortedByDescending { it.goals.size }
            topScorersRecycler.adapter = topPlayersAdapter
        }
        async(UI) {
            val fixtures = async { Model.getFixtures() }.await()
            val adapter = FixturesPagerAdapter(fixtures.sortedBy { it.date }, this@MainFragment)
            fixturesViewPager.adapter = adapter
            fixturesViewPager.offscreenPageLimit = fixtures.count()
            fixturesViewPager.currentItem = fixtures.indexOf(fixtures.last())
        }
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun onFixtureClick(fixture: Fixture) {
        val intent = Intent(context, FixtureActivity::class.java)
        intent.putExtra("fixture", fixture)
        startActivity(intent)
    }

    companion object {
        fun newInstance(): MainFragment {
            val fragment = MainFragment()
            return fragment
        }
    }
}
