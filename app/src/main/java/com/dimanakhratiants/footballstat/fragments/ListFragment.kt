package com.dimanakhratiants.footballstat.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.dimanakhratiants.footballstat.R
import com.dimanakhratiants.footballstat.activities.FixtureActivity
import com.dimanakhratiants.footballstat.activities.TeamActivity
import com.dimanakhratiants.footballstat.adapters.FixturesAdapter
import com.dimanakhratiants.footballstat.adapters.TeamsAdapter
import com.dimanakhratiants.footballstat.dialogs.AddFixtureDialog
import com.dimanakhratiants.footballstat.dialogs.AddTeamDialog
import com.dimanakhratiants.footballstat.entity.Fixture
import com.dimanakhratiants.footballstat.entity.Team
import com.dimanakhratiants.footballstat.model.Model
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async

/**
 * A simple [Fragment] subclass.
 * Use the [TeamFreagment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListFragment : Fragment(),
        FixturesAdapter.FixtureClickListener,
        TeamsAdapter.TeamClickListener,
        AddTeamDialog.AddTeamListener,
        FixturesAdapter.FixtureDeleteListener,
        TeamsAdapter.TeamDeleteListener,
        AddFixtureDialog.AddFixtureListener{

    enum class Type{
        TEAM, FIXTURE
    }
    var type: Type? = null
    lateinit var recyclerView: RecyclerView
    lateinit var layout: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.type = Type.valueOf(arguments.getString("type"))
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        layout = inflater!!.inflate(R.layout.fragment_list, container, false)
        layout?.findViewById<SwipeRefreshLayout>(R.id.swipeRefreshLayout).setOnRefreshListener {
            updateUI(layout)
            swipeRefreshLayout.isRefreshing = false
        }
        return  layout
    }

    override fun onResume() {
        super.onResume()
        updateUI(layout)
    }
    private fun updateUI(view: View) {
        recyclerView = view.findViewById<RecyclerView>(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(context)

        when (type) {
            Type.TEAM -> {
                view.findViewById<FloatingActionButton>(R.id.floatingActionButton).setOnClickListener {
                    AddTeamDialog.newInstance(this).show(fragmentManager, "ADD TEAM")
                }
                async(UI) {
                    val teams = async { Model.getTeams() }.await() as ArrayList
                    val adapter = TeamsAdapter(teams, this@ListFragment, this@ListFragment)
                    recyclerView.adapter = adapter
                }
            }
            Type.FIXTURE -> {
                view.findViewById<FloatingActionButton>(R.id.floatingActionButton).setOnClickListener {
                    AddFixtureDialog.newInstance(this).show(fragmentManager, "ADD FIIXTURE")
                }
                async(UI) {
                    val fixtures = async { Model.getFixtures() }.await() as ArrayList
                    val adapter = FixturesAdapter(fixtures, this@ListFragment, this@ListFragment)
                    recyclerView.adapter = adapter
                }
            }
        }
    }

    override fun onTeamClick(team: Team) {
        val intent = Intent(context, TeamActivity::class.java)
        intent.putExtra("team", team)
        startActivity(intent)
    }

    override fun onFixtureClick(fixture: Fixture) {
        val intent = Intent(context, FixtureActivity::class.java)
        intent.putExtra("fixture", fixture)
        startActivity(intent)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
    }

    override fun onDetach() {
        super.onDetach()
    }

    override fun addTeam(team: Team) {
        async(UI) {
           val team = async { Model.addTeam(team)}.await()
            val adapter = recyclerView.adapter as TeamsAdapter
            adapter.teams.add(team)
            adapter.notifyDataSetChanged()
        }
    }

    override fun addFixture(fixture: Fixture) {
        async(UI) {
            val fixture = async { Model.addFixture(fixture)}.await()
            val adapter = recyclerView.adapter as FixturesAdapter
            adapter.fixtures.add(fixture)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onFixtureDelete(fixture: Fixture) {
        async(UI){
            async { Model.deleteFixture(fixture) }.await()
            val adapter = this@ListFragment.recyclerView.adapter as FixturesAdapter
            adapter.fixtures.remove(fixture)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onTeamDelete(team: Team) {
        async(UI){
            async { Model.deleteTeam(team) }.await()
            val adapter = this@ListFragment.recyclerView.adapter as TeamsAdapter
            adapter.teams.remove(team)
            adapter.notifyDataSetChanged()
        }
    }

    companion object {
        fun newInstance(type: Type): ListFragment {
            val fragment = ListFragment()
            val bundle =  Bundle()
            bundle.putString("type", type.name)
            fragment.arguments = bundle
            return fragment
        }
    }
}
