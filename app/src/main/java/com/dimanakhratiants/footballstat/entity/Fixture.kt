package com.dimanakhratiants.footballstat.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by dima on 29.11.2017.
 */

data class Fixture(
        @SerializedName("id")var id: Long? = null,
        @SerializedName("homeTeam")val homeTeam: Team?,
        @SerializedName("awayTeam")val awayTeam: Team?,
        @SerializedName("homeGoals")var homeGoals: Int = 0,
        @SerializedName("awayGoals")var awayGoals: Int =  0,
        @SerializedName("date")val date: Long,
        @SerializedName("goals")var goals: List<Goal> = ArrayList()
): Serializable