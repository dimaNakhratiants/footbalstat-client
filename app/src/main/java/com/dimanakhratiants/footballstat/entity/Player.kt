package com.dimanakhratiants.footballstat.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by dima on 29.11.2017.
 */


data class Player(
        @SerializedName("id")val id: Long? = null,
        @SerializedName("name")var name: String,
        @SerializedName("position")var position: String,
        @SerializedName("nationality")var nationality: String,
        @SerializedName("dateOfBirth")var dateOfBirth: Long,
        @SerializedName("teamTitle")var teamTitle: String? = null,
        @SerializedName("team")var team: Team? = null,
        @SerializedName("goals")var goals: List<Goal> = ArrayList()
): Serializable