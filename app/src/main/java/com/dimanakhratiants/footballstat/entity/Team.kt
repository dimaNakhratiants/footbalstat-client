package com.dimanakhratiants.footballstat.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by dima on 29.11.2017.
 */
data class Team(
        @SerializedName("id")var id: Long? = null,
        @SerializedName("title")val title: String,
        @SerializedName("players")var players: List<Player> = ArrayList()
): Serializable