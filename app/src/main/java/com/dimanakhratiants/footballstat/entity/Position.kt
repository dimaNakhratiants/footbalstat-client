package com.dimanakhratiants.footballstat.entity

/**
 * Created by dima on 29.11.2017.
 */
enum class Position{
    GOALKEEPER, DEFENDER, MIDDLEFIELDER, FORWARD;

    fun abbr(): String{
        return when(this){
            GOALKEEPER -> "GK"
            DEFENDER -> "DF"
            MIDDLEFIELDER -> "MD"
            FORWARD -> "FW"
        }
    }
}
