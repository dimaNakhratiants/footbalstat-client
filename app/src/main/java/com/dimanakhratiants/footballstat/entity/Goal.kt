package com.dimanakhratiants.footballstat.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by dima on 29.11.2017.
 */

data class Goal(
        @SerializedName("id")val id: Long? = null,
        @SerializedName("minute")val minute: Int,
        @SerializedName("fixture")val fixture: Fixture? = null,
        @SerializedName("player")var player: Player? = null,
        @SerializedName("playerName")var playerName: String
): Serializable